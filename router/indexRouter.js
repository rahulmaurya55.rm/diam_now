const express = require("express");
const router=express.Router();
const LoginRegister=require("../controller/LoginRegisterController");

router.get("/:id",(request,response)=>{

    console.log(request.params.id);
    response.status(200);
    response.json({
        message:"get",
    })
})

router.post("/",(request,response)=>{

    // console.log(request);

    LoginRegister.register(request,response);

    // response.status(200);
    // response.json({
    //     message:"post",
    //     data:request.body
    // })
})

router.post("/login",(request,response)=>{

    // console.log(request);

    LoginRegister.login(request,response);

    // response.status(200);
    // response.json({
    //     message:"post",
    //     data:request.body
    // })
})


module.exports=router;