const express=require("express");
// const bodyParser = require('body-parser')

const app=express();


app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const indexRouter=require("./router/indexRouter");


app.listen(3001,()=>{
    // console.log("port connected");
})




// var bodyParser = require('body-parser');
// app.use(express.urlencoded());
// app.use(bodyParser.json());


app.use('/register',indexRouter);


